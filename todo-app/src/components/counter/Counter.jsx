import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Counter.css'


class Counter extends Component {

  constructor() {
    super();
    this.state = {
      counter: 0
    }

    this.increment = this.increment.bind(this);
    this.Reset = this.Reset.bind(this);
  }


  render() {
    return (
      <div className="Counter">
        <CounterButton by={1} incrementMethod={this.increment} />
        <CounterButton by={5} incrementMethod={this.increment} />
        <CounterButton by={10} incrementMethod={this.increment} />
        <span className="count">{this.state.counter}</span>
        <div><button className="reset" onClick={this.Reset}>Reset
          </button></div>
      </div>
    );
  }

  Reset() {
    // console.log(`incrementt from child to parent counter ${by}`);
    this.setState(
      { counter: 0 }
    );
  }
  increment(by) {
    // console.log(`incrementt from child to parent counter ${by}`);
    this.setState(
      (prevState) => {
        return { counter: prevState.counter + by }
      }
    );
  }
}


class CounterButton extends Component {
  //define the initial state in a constructor
  //state=0

  constructor() {
    super();
    // this.state = {
    //   counter: 0
    // }
    // this.increment = this.increment.bind(this);
    // this.decrement = this.decrement.bind(this);
  }

  render() {
    return (
      <div className="counter">
        <button onClick={()=>this.props.incrementMethod(this.props.by)}>+{this.props.by}</button>
        <button onClick={()=>this.props.incrementMethod(-this.props.by)}>-{this.props.by}</button>
        {/* <span className="count">{this.state.counter}</span>*/}
      </div>
    );
  }

  // increment() {//Update State-counter +1
  //   //console.log('increment');
  //   this.setState({
  //     counter: this.state.counter + this.props.by
  //   });

  //   this.props.incrementMethod(this.props.by);
  // }

  // decrement() {//Update State-counter +1
  //   //console.log('increment');
  //   this.setState({
  //     counter: this.state.counter - this.props.by
  //   });

  //   this.props.incrementMethod(-this.props.by);
  // }
}

CounterButton.defaultProps = {
  by: 1
}
/*Counter.protoTypes ={

  by:PropTypes.number
}*/

export default Counter;
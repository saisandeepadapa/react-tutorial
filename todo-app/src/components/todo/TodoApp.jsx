import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom'
class TodoApp extends Component {
    render() {
        return (
            <div className="TodoApp">

                <Router>
                    <>
                        <HeaderComponent />
                        <Switch>
                            <Route path="/" exact component={LoginComponent} />
                            <Route path="/login" component={LoginComponent} />
                            <Route path="/welcome/:name" component={WelcomeComponent} />
                            <Route path="/todos" component={ListTodosComponent} />
                            <Route path="/logout" component={LogoutComponent} />
                            <Route component={ErrorComponent} />
                        </Switch>
                        <FooterComponent />
                    </>
                </Router>
                {/*<LoginComponent />
                <WelcomeComponent/>*/}
            </div>
        )
    }
}

class HeaderComponent extends Component {
    render() {
        return (
            <header>

                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div>
                        <a href="http://www.google.com" className="navbar-brand"> saisandeepadpa</a>
                    </div>
                    <ul className="navbar-nav">
                        <li ><Link className="nav-link" to="/welcome/saisandeepadapa">Home</Link></li>
                        <li ><Link className="nav-link" to="/todos">Todos</Link></li>
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">
                        <li ><Link className="nav-link" to="/login">Login</Link></li>
                        <li ><Link className="nav-link" to="/logout">Logout</Link></li>
                    </ul>
                </nav>
            </header>


        )
    }
}

class FooterComponent extends Component {
    render() {
        return (
            <footer className="footer">
                <span className="text-muted">
                    All Rights Reserved 2020 @saisandeepadapa
                </span>
            </footer>
        )
    }
}

class LogoutComponent extends Component {
    render() {
        return (
            <div>
                <h1>You Are logged out</h1>
                <div className="container">
                    Thank You for using our apllication.
                </div>
            </div>
        )
    }
}

class ListTodosComponent extends Component {
    constructor() {
        super()
        this.state = {
            todos:
                [
                    { id: 1, description: 'Learn React', done: false, targetDate: new Date() },
                    { id: 2, description: 'Learn manners', done: false, targetDate: new Date() },
                    { id: 3, description: 'Learn howtoreact', done: false, targetDate: new Date() },
                    { id: 4, description: 'Learn coolness', done: false, targetDate: new Date() }
                ]
        }
    }



    render() {
        return (


            <div>
                <h1>List Todos</h1>
                <div className="container">
                <table class="table">
                    <thead>
                        <tr>
                           {/* <th>id</th>*/}
                            <th>description</th>
                            <th>Target Date</th>
                            <th>Is Completed</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.todos.map(
                                todo =>
                                    <tr>
                                        {/*<td>{todo.id}</td>*/}
                                        <td>{todo.description}</td>
                                        <td>{todo.done.toString()}</td>
                                        <td>{todo.targetDate.toString()}</td>
                                    </tr>
                            )

                        }
                    </tbody>
                </table>
                </div>
            </div>
        )
    }
}

class WelcomeComponent extends Component {
    render() {
        return (
            <>
            <h1>Welcome!</h1>
            <div class="container">Welcome {this.props.match.params.name}
                You can manage your todos <Link to="/todos" >here</Link></div>
            </>
        )
    }
}

function ErrorComponent() {
    return <div>An error occurred</div>
}


class LoginComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: "saisandeepadapa",
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false

        }
        //this.handleUsernameChange = this.handleUsernameChange.bind(this)
        //this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.loginClicked = this.loginClicked.bind(this)
    }

    handleChange(event) {
        console.log(this.state)
        this.setState({ [event.target.name]: event.target.value })
    }

    loginClicked(event) {
        // console.log(this.state)
        if (this.state.username === 'saisandeepadapa' && this.state.password === 'dummy') {
            this.props.history.push(`/welcome/${this.state.username}`)
            console.log("successfull")
            this.setState({ showSuccessMessage: true })
            this.setState({ hasLoginFailed: false })

        } else {
            console.log("fail");
            this.setState({ hasLoginFailed: true })
            this.setState({ showSuccessMessage: false })
        }
    }

    // handleUsernameChange(event) {
    //     console.log(event.target.value)
    //     this.setState({ [event.target.name]: event.target.value })
    // }

    // handlePasswordChange(event) {
    //     console.log(event.target.value)
    //     this.setState({ password: event.target.value })
    // }

    render() {
        return (
            <div>
                <h1>Login</h1>
                <div className="container">
                    {/*<ShowinvalidCredentials hasLoginFailed={this.state.hasLoginFailed} />*/}
                    {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid Credentials</div>}
                    {/*<ShowLoginSuccessMessage showSuccessMessage={this.state.showSuccessMessage} />*/}
                    {this.state.showSuccessMessage && <div>Login Successful</div>}
                    User Name: <input type="text" name="username" value={this.state.username}
                        onChange={this.handleChange} />
                    Password: <input type="password" name="password" value={this.state.password}
                        onChange={this.handleChange} />
                    <button className="btn btn-success" onClick={this.loginClicked}>Login</button>
                </div>
            </div>
        )
    }
}

function ShowinvalidCredentials(props) {
    if (props.hasLoginFailed) {
        return <div>Invalid Credentials</div>
    }
    return null
}

function ShowLoginSuccessMessage(props) {
    if (props.showSuccessMessage) {
        return <div>Login Successful</div>
    }
    return null
}
export default TodoApp